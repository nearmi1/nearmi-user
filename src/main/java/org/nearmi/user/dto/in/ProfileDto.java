package org.nearmi.user.dto.in;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

@Data
public class ProfileDto {
    private String email;
    private String name;
    private String familyName;
    @DateTimeFormat
    private LocalDateTime birthdate;
    private String genderCode;
}
