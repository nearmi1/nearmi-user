package org.nearmi.user.dto.in;

import lombok.Data;

/**
 * unicity dto used to check if a field value is unique in database
 * @author adjebarri
 * @since 0.1.0
 */
@Data
public class UnicityDto {
    private String field;
    private String value;
}
