package org.nearmi.user.repository.custom.impl;

import org.nearmi.core.mongo.document.MiUser;
import org.nearmi.user.repository.custom.MiUserCustomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;

/**
 * implementation of {@link MiUserCustomRepository}
 *
 * @author adjebarri
 * @since 0.1.0
 */
public class MiUserRepositoryImpl extends UnicityRepositoryImpl implements MiUserCustomRepository {
    @Autowired
    private MongoTemplate template;

    @Override
    public boolean isUnique(String value, String field) {
        return this.isUnique(value, field, MiUser.class);
    }
}
