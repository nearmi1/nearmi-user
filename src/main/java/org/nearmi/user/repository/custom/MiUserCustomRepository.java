package org.nearmi.user.repository.custom;

/**
 * custom {@link org.nearmi.core.mongo.document.MiUser} repository<br/>
 * contains database access method implemented manually
 * @author adjebarri
 * @since 0.1.0
 */
public interface MiUserCustomRepository {
    /**
     * crud method to check if value exists in MiUserClass
     * @param value filed value
     * @param field filed name
     * @return true if value doesn't exits for another document (specified field) false neiher
     */
    boolean isUnique(String value, String field);
}
