package org.nearmi.user.repository;

import org.bson.types.ObjectId;
import org.nearmi.core.mongo.document.MiUser;
import org.nearmi.user.repository.custom.MiUserCustomRepository;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MiUserRepository extends MongoRepository<MiUser, ObjectId>, MiUserCustomRepository {
}
