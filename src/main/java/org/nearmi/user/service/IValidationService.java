package org.nearmi.user.service;

/**
 * validation service, it provides some utility to validate user data
 * @author adjebarri
 * @since  0.1.0
 */
public interface IValidationService {
    /**
     * check if field is unique or not
     * @param value the searched value
     * @param field the field holding the value
     * @return true if unique false neither
     */
    boolean isUnique(String value, String field);
}
