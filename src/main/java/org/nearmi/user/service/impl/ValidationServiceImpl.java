package org.nearmi.user.service.impl;

import org.nearmi.user.repository.MiUserRepository;
import org.nearmi.user.service.IValidationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static org.nearmi.core.validator.Validator.notNull;

/**
 * @author adjebarri
 * @since 0.1.0
 */
@Service
public class ValidationServiceImpl implements IValidationService {
    @Autowired
    private MiUserRepository userRepository;

    @Override
    public boolean isUnique(String value, String field) {
        notNull(field, "field");
        notNull(value, "value");
        return userRepository.isUnique(value, field);
    }
}
