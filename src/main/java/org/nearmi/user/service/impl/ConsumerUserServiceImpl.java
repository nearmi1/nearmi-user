package org.nearmi.user.service.impl;

import org.nearmi.core.mongo.document.MiUser;
import org.nearmi.core.security.CoreSecurity;
import org.nearmi.user.dto.in.ProfileDto;
import org.nearmi.user.repository.MiUserRepository;
import org.nearmi.user.service.IConsumerUserService;
import org.nearmi.user.service.IValidationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static org.nearmi.core.validator.Validator.notNull;

/**
 * implementation of {@link IConsumerUserService}
 *
 * @author adjebarri
 * @since 0.1.0
 */
@Service
public class ConsumerUserServiceImpl implements IConsumerUserService {
    @Autowired
    private MiUserRepository userRepository;
    @Autowired
    private IValidationService validationService;

    @Override
    public void saveProfile(ProfileDto profile) {
        notNull(profile.getName(), "name");
        notNull(profile.getFamilyName(), "familyName");
        notNull(profile.getEmail(), "email");
        String username = CoreSecurity.getAttribute("username");
        MiUser mi = new MiUser();
        mi.setUsername(username);
        mi.setBirthdate(mi.getBirthdate());
        mi.setName(profile.getName());
        mi.setFamilyName(profile.getFamilyName());
        mi.setEmail(profile.getEmail());
        userRepository.save(mi);
    }
}
