package org.nearmi.user.service;

import org.nearmi.user.dto.in.ProfileDto;

/**
 * <p>this class holds business code for users (consumers) such as profile completion <br/>
 * or appointments processing...</p>
 * @author adjebarri
 * @since 0.1.0
 */
public interface IConsumerUserService {
    /**
     * method used to persist user information when registration occurs
     * @param profile an object holding user information
     */
    void saveProfile(ProfileDto profile);
}
