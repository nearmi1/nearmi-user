package org.nearmi.user.rest;

import org.nearmi.user.dto.in.ProfileDto;
import org.nearmi.user.dto.in.UnicityDto;
import org.nearmi.user.service.IConsumerUserService;
import org.nearmi.user.service.IValidationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/users")
@RestController
public class UserController {
    @Autowired
    private IValidationService validationService;
    @Autowired
    private IConsumerUserService consumerUserService;

    @PostMapping("/save-profile")
    public ResponseEntity<Void> save(@RequestBody ProfileDto profile) {
        consumerUserService.saveProfile(profile);
        return ResponseEntity.status(201).build();
    }

    @PostMapping("/unique")
    public ResponseEntity<Boolean> isUnique(@RequestBody UnicityDto body) {
        boolean res = validationService.isUnique(body.getValue(), body.getField());
        return ResponseEntity.ok(res);
    }
}
